﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_2
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Каждая введенная строка будет добавляться к предыдущей, пока не будет введено end или конец");
            string sentence = "";
            for (; ; )
            {
                Console.WriteLine("Введите строку:");
                string name = Console.ReadLine();
                Console.WriteLine(name.ToUpper());
                string[] terms = { "end", "конец" };
                bool quitting = false;
                foreach (string term in terms)
                {
                    if (String.Compare(name, term) == 0)
                    {
                        quitting = true;
                    }
                }
                if (quitting == true)
                {
                    break;
                }
                sentence = String.Concat(sentence, name);
                Console.WriteLine("\nПолученная строка:" + sentence);
            }
                Console.WriteLine("\nИтоговое выражение: \n" + sentence);
                Console.WriteLine("Нажмите любую клавишу для завершения программы");
                Console.Read(); 
        }
    }
}
